package com.pc.mock.subchannel.exceptions;

import com.pc.mock.subchannel.constant.ErrorCode;
import com.pc.mock.subchannel.constant.ErrorMessage;

public class InvalidJsonException extends ApplicationException {

  public InvalidJsonException(Throwable cause) {
    super(ErrorCode.INVALID_JSON_PAYLOAD, ErrorMessage.INVALID_JSON_PAYLOAD, cause);
  }
}
