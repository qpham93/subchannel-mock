package com.pc.mock.subchannel.config;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

/**
 * Init queues to bind message from exchanges with correct routing keys for local or test envs.
 */
@Validated
@Configuration
@RequiredArgsConstructor
public class InitQueuesConfig {

  private final TopicExchange notiExchange;

  private final MessagingProperties messagingProperties;

  @Bean
  public Queue channelNewEmailSubchannelQueue() {
    return QueueBuilder.durable(messagingProperties.getChannelNewEmailSubchannelQueue())
        .build();
  }

  @Bean
  public Binding bindChannelNewEmailSubchannelQueueToNotiExchange(Queue channelNewEmailSubchannelQueue) {
    return BindingBuilder
        .bind(channelNewEmailSubchannelQueue)
        .to(notiExchange)
        .with(messagingProperties.getRoutingKey().getChannelNewEmailSubchannel());
  }

}