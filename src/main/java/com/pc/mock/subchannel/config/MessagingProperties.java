package com.pc.mock.subchannel.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@Validated
@ConstructorBinding
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "application.messaging")
@Component
public class MessagingProperties {

  @NotNull
  @Valid
  private ListenerConfig listener;

  @NotBlank
  private String notiExchange;

  @NotBlank
  private String channelNewEmailSubchannelQueue;

  @NotNull
  @Valid
  private RoutingKey routingKey;

  @Getter
  @RequiredArgsConstructor
  public static class RoutingKey {

    @NotBlank
    private final String channelNewEmailSubchannel;

    @NotBlank
    private final String subchannelUpdateEmailChannel;
  }

  @Getter
  @RequiredArgsConstructor
  public static class ListenerConfig {

    @NotBlank
    private final String concurrency;

    @NotBlank
    private final String ackMode;
  }

}
