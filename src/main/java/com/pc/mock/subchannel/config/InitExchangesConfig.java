package com.pc.mock.subchannel.config;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

/**
 * Init exchanges for local or test environments.
 */
@Validated
@Configuration
@RequiredArgsConstructor
public class InitExchangesConfig {

  private final MessagingProperties messagingProperties;

  @Bean
  public TopicExchange notiExchange() {
    return new TopicExchange(messagingProperties.getNotiExchange());
  }
}
