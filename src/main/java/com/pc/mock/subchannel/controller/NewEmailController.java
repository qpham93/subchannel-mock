package com.pc.mock.subchannel.controller;

import com.pc.mock.subchannel.payload.EmailRequest;
import com.pc.mock.subchannel.payload.EmailResponse;
import com.pc.mock.subchannel.service.MessagingService;
import com.pc.mock.subchannel.service.PayloadValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Controller that handles new email requests
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class NewEmailController {

  @Value("${application.transaction.status}")
  private String status;

  private static final String requestType = "update_email_status";

  private final PayloadValidator payloadValidator;
  private final MessagingService messagingService;

  public void process(String payload) {
    try {
      EmailRequest request = payloadValidator.parseJson(payload, EmailRequest.class);
      messagingService.sendEmailResponseToChannel(EmailResponse.builder()
          .outgoingEmailTransactionId(request.getOutgoingEmailTransactionId())
          .status(status)
          .requestType(requestType).build());
    } catch (Exception e) {
      log.error("Exception: ", e);
    }
  }

}
