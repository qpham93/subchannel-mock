package com.pc.mock.subchannel;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pc.mock.subchannel.mapper.IgnoreTypeHeaderMapper;
import javax.validation.Validation;
import javax.validation.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.api.RabbitListenerErrorHandler;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.amqp.RabbitTemplateConfigurer;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;

@Slf4j
@SpringBootApplication
@ConfigurationPropertiesScan
public class SubchannelMockApplication {

  public static void main(String[] args) {
    SpringApplication.run(SubchannelMockApplication.class, args);
  }

  @Bean
  RabbitListenerErrorHandler processRequestErrorHandler() {
    return (amqpMessage, message, exception) -> {
      log.error("processNewRequestErrorHandler", exception);
      return null;
    };
  }

  @Bean
  Jackson2JsonMessageConverter amqpJsonMessageConverter(ObjectMapper objectMapper) {
    final Jackson2JsonMessageConverter jackson2JsonMessageConverter = new Jackson2JsonMessageConverter(
        objectMapper);
    jackson2JsonMessageConverter.setJavaTypeMapper(new IgnoreTypeHeaderMapper());
    return jackson2JsonMessageConverter;
  }

  @Bean
  public RabbitTemplate rabbitTemplate(RabbitTemplateConfigurer configurer,
      ConnectionFactory connectionFactory) {
    final var template = new RabbitTemplate();

    template.addBeforePublishPostProcessors(message -> {
      final var messageBody = new String(message.getBody());

      log.info("Publishing message: {}", messageBody);

      return message;
    });

    configurer.configure(template, connectionFactory);

    return template;
  }

  @Bean
  public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(
      SimpleRabbitListenerContainerFactoryConfigurer configurer,
      ConnectionFactory connectionFactory) {
    final var factory = new SimpleRabbitListenerContainerFactory();

    configurer.configure(factory, connectionFactory);
    factory.setAfterReceivePostProcessors(message -> {
      final var messageBody = new String(message.getBody());
      final var messageQueue = message.getMessageProperties().getConsumerQueue();

      log.info("Receiving message from queue {} with content: {}", messageQueue, messageBody);

      return message;
    });

    return factory;
  }
}
