package com.pc.mock.subchannel.service;

import com.pc.mock.subchannel.payload.EmailResponse;

/**
 * Define method interfaces to communicate with message queues
 */
public interface MessagingService {

  void sendEmailResponseToChannel(EmailResponse payload);
}
