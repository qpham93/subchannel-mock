package com.pc.mock.subchannel.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pc.mock.subchannel.exceptions.InvalidJsonException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class PayloadValidator {

  private final ObjectMapper objectMapper;

  public <T> T parseJson(String data, Class<T> clazz) throws InvalidJsonException {
    try {
      return objectMapper.readValue(data, clazz);
    } catch (JsonProcessingException e) {
      throw new InvalidJsonException(e);
    }
  }
}
