package com.pc.mock.subchannel.service.impl;

import com.pc.mock.subchannel.config.MessagingProperties;
import com.pc.mock.subchannel.payload.EmailResponse;
import com.pc.mock.subchannel.service.MessagingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MessagingServiceImpl implements MessagingService {

  private final RabbitTemplate rabbitTemplate;

  private final MessagingProperties messagingProperties;

  @Override
  public void sendEmailResponseToChannel(EmailResponse payload) {
    log.info("Sending payload response to channel: {}", payload);

    rabbitTemplate.convertAndSend(
        messagingProperties.getNotiExchange(),
        messagingProperties.getRoutingKey().getSubchannelUpdateEmailChannel(),
        payload);
  }

}
