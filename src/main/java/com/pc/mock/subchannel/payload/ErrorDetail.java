package com.pc.mock.subchannel.payload;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class ErrorDetail {
  private final Integer errorCode;

  private final String errorMessage;
}
