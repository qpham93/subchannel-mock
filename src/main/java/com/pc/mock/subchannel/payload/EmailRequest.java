package com.pc.mock.subchannel.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmailRequest {

  @NotNull
  private Integer outgoingEmailTransactionId;
}
