package com.pc.mock.subchannel.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ErrorCode {

  public static final int INVALID_JSON_PAYLOAD = 1;
}
