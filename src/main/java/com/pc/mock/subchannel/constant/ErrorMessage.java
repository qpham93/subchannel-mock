package com.pc.mock.subchannel.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ErrorMessage {

  public static final String INVALID_JSON_PAYLOAD = "Invalid payload";
}
