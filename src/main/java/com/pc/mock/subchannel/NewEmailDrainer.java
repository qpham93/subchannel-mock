package com.pc.mock.subchannel;

import com.pc.mock.subchannel.controller.NewEmailController;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class NewEmailDrainer {

  private final NewEmailController newEmailController;

  @RabbitListener(
      queues = "#{channelNewEmailSubchannelQueue}",
      concurrency = "#{messagingProperties.listener.concurrency}",
      errorHandler = "processRequestErrorHandler",
      ackMode = "#{messagingProperties.listener.ackMode}"
  )
  public void processNewTransactionRequest(Message message) {
    newEmailController.process(new String(message.getBody()));
  }
}
